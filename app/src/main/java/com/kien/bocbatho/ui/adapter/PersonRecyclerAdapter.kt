package com.kien.bocbatho.ui.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.OvershootInterpolator
import android.view.animation.RotateAnimation
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kien.bocbatho.R
import com.kien.bocbatho.data.entities.Person
import com.kien.bocbatho.util.toMoneyString
import kotlinx.coroutines.delay

class PersonRecyclerAdapter(private val context: Context): RecyclerView.Adapter<PersonViewHolder>(), Filterable {
    private var listener: ((person: Person) -> Unit)? = null
    private var deleteListener: ((Person) -> Unit)? = null
    private var items = listOf<Person>()
    private var itemsFiltered = items.toMutableList()
    private var currentFilterString = ""
    var isDeleting = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_item, parent, false)
        return PersonViewHolder(view)
    }

    override fun onBindViewHolder(holderPerson: PersonViewHolder, position: Int) {
        holderPerson.name.text = itemsFiltered[position].name.let {
            if (it.isBlank()) {
                holderPerson.name.setTextColor(context.resources.getColor(R.color.black_secondary))
                holderPerson.name.setTypeface(holderPerson.name.typeface, Typeface.ITALIC)
                context.resources.getString(R.string.ui_no_name)
            } else {
                holderPerson.name.setTextColor(context.resources.getColor(R.color.black))
                holderPerson.name.setTypeface(null, Typeface.NORMAL)
                it
            }
        }
        holderPerson.subName.text = itemsFiltered[position].subName
        holderPerson.money.text = itemsFiltered[position].moneyPerDay.toMoneyString()
        holderPerson.btnDelete.setOnClickListener {
            deleteListener?.invoke(itemsFiltered[position])
        }

        holderPerson.itemView.setOnClickListener {
            if (isDeleting) {
                deleteOff()
            } else {
                this@PersonRecyclerAdapter.listener?.invoke(itemsFiltered[position])
            }
        }

        holderPerson.itemView.setOnLongClickListener {
            deleteOn()
            true
        }

        if (isDeleting) {
            val shakeAnim = RotateAnimation(-0.3f, 0.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
                duration = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
                interpolator = OvershootInterpolator()
                repeatCount = Animation.INFINITE
            }
            holderPerson.itemView.startAnimation(shakeAnim)
            holderPerson.btnDelete.visibility = View.VISIBLE
        } else {
            holderPerson.itemView.animation = null
            holderPerson.btnDelete.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int = itemsFiltered.size

    override fun getFilter(): Filter {
        return object: Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                itemsFiltered.clear()
                p0?.also {
                    currentFilterString = p0.toString()
                    val text = it.toString().toLowerCase()
                    if (text.isEmpty() || text.isBlank()) {
                        itemsFiltered = items.toMutableList()
                    } else {
                        items.forEach {person ->
                            if (person.name.toLowerCase().contains(text) || (person.subName != null && person.subName!!.toLowerCase().contains(text))) {
                                itemsFiltered.add(person)
                            }
                        }
                    }
                }
                val result = FilterResults()
                result.values = itemsFiltered
                return result
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                p1?.also {
                    itemsFiltered = it.values as MutableList<Person>
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun setOnItemClickListener(listener: (person: Person) -> Unit) {
        this.listener = listener
    }

    fun setData(data: List<Person>) {
        this.items = data
        filter.filter(currentFilterString)
    }

    fun setOnItemDeleteListener(listener: (Person) -> Unit) {
        this.deleteListener = listener
    }

    private fun deleteOn() {
        if (!isDeleting) {
            isDeleting = true
            notifyDataSetChanged()
        }
    }

    fun deleteOff() {
        if (isDeleting) {
            isDeleting = false
            notifyDataSetChanged()
        }
    }
}

class PersonViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val name: TextView = view.findViewById(R.id.tv_name)
    val subName: TextView = view.findViewById(R.id.tv_sub_name)
    val money: TextView = view.findViewById(R.id.tv_money)
    val btnDelete: ImageButton = view.findViewById(R.id.btn_delete)
}