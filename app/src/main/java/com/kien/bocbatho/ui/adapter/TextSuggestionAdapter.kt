package com.kien.bocbatho.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView

class TextSuggestionAdapter(private val context: Context, private val dataSource: List<String>): BaseAdapter(), Filterable {
    val data = mutableListOf<String>()
    private val filteredData = mutableListOf<String>()

    init {
        data.addAll(dataSource)
        filteredData.addAll(dataSource)
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(android.R.layout.simple_dropdown_item_1line, p2, false)
        view.findViewById<TextView>(android.R.id.text1).text = filteredData[p0]
        return view
    }

    override fun getItem(p0: Int): Any = filteredData[p0]

    override fun getItemId(p0: Int): Long = p0.toLong()

    override fun getCount(): Int = filteredData.size

    override fun getFilter(): Filter {
        return object: Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val filtered = if (p0.toString().isEmpty() || p0.toString().isBlank()) {
                    data
                } else {
                    data.filter {
                        it.contains(p0.toString())
                    }
                }
                val res = FilterResults()
                res.values = filtered
                return res
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                filteredData.clear()
                filteredData.addAll(p1?.values as List<String>)
                notifyDataSetChanged()
            }
        }
    }

    fun add(str: String) {
        if (!data.contains(str)) {
            data.add(str)
            notifyDataSetChanged()
        }
    }
}