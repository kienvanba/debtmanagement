package com.kien.bocbatho.ui.base

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.animation.addListener
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.kien.bocbatho.R
import com.kien.bocbatho.util.defIntValue
import com.kien.bocbatho.util.isNotDefValue
import kotlin.math.sqrt

abstract class BaseAnimActivity: BaseActivity() {
    companion object {
        const val animAtX: String = "_x_position"
        const val animAtY: String = "_y_position"
    }

    private var rootView: View? = null
    private var startX: Int = defIntValue
    private var startY: Int = defIntValue

    override fun onCreate(savedInstanceState: Bundle?) {
        theme.applyStyle(R.style.AppTheme_Animation, true)
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null && intent.hasExtra(animAtX) && intent.hasExtra(animAtY)) {
            startX = intent.getIntExtra(animAtX, defIntValue)
            startY = intent.getIntExtra(animAtY, defIntValue)
        }
    }

    protected fun getRootView() {
        rootView = findViewById(android.R.id.content)
    }

    protected fun revealWithAnimation() {
        if (startX.isNotDefValue() && startY.isNotDefValue()) {
            animIn()
        } else {
            toast("anim position is not initialized")
        }
    }

    protected fun finishWithAnimation() {
        if (startX.isNotDefValue() && startY.isNotDefValue()) {
            animOut()
        } else {
            toast("anim position is not initialized")
        }
    }

    private fun animIn() {
        if (rootView != null) {
            rootView!!.addOnLayoutChangeListener(object: View.OnLayoutChangeListener {
                override fun onLayoutChange(
                    p0: View?,
                    p1: Int,
                    p2: Int,
                    p3: Int,
                    p4: Int,
                    p5: Int,
                    p6: Int,
                    p7: Int,
                    p8: Int
                ) {
                    rootView!!.removeOnLayoutChangeListener(this)
                    val finalRadius =
                        sqrt((rootView!!.width * rootView!!.width).toFloat() + (rootView!!.height * rootView!!.height).toFloat())
                    val anim = ViewAnimationUtils.createCircularReveal(rootView!!, startX, startY, 0f, finalRadius)
                    anim.duration =
                        resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
                    anim.interpolator = FastOutSlowInInterpolator()
                    anim.start()
                    animColor(rootView!!, resources.getColor(R.color.colorAccent), resources.getColor(android.R.color.white))
                }
            })
        } else {
            toast("root view is not initialized")
        }
    }

    private fun animOut() {
        if (rootView != null) {
            val finalRadius =
                sqrt((rootView!!.width * rootView!!.width).toFloat() + (rootView!!.height * rootView!!.height).toFloat())
            val anim = ViewAnimationUtils.createCircularReveal(rootView!!, startX, startY, finalRadius, 0f)
            anim.duration =
                resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
            anim.interpolator = FastOutSlowInInterpolator()
            anim.addListener(onEnd = {
                rootView!!.visibility = View.INVISIBLE
                this@BaseAnimActivity.finish()
            })
            anim.start()
        } else {
            toast("root view is not initialized")
        }
    }

    private fun animColor(view: View, startColor: Int, endColor: Int) {
        val anim = ValueAnimator()
        anim.setIntValues(startColor, endColor)
        anim.setEvaluator(ArgbEvaluator())
        anim.addUpdateListener {
            view.setBackgroundColor(it.animatedValue as Int)
        }
        anim.duration = this@BaseAnimActivity.resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
        anim.start()
    }
}