package com.kien.bocbatho.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.animation.AlphaAnimation
import com.kien.bocbatho.R
import com.kien.bocbatho.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val loadAnim = AlphaAnimation(0f, 1f).apply {
            duration = 2500
            fillAfter = true
            isFillEnabled = true
        }
        image.startAnimation(loadAnim)
        Handler().postDelayed({
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            this.finish()
        }, 3500)
    }
}