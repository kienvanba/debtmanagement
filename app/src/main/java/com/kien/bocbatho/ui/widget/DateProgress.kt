package com.kien.bocbatho.ui.widget

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.kien.bocbatho.R
import com.kien.bocbatho.util.toPixel
import org.joda.time.DateTime
import org.joda.time.Days

class DateProgress(context: Context): View(context) {
    constructor(context: Context, attributeSet: AttributeSet): this(context)

    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int): this(context, attributeSet)

    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int, defStyleRes: Int): this(context, attributeSet, defStyle)

    private val paint: Paint = Paint()
    private val minimumProgress = width.toFloat()*0.05f
    private var progress = minimumProgress

    var startDate: DateTime? = null
    var duration: Int = 0

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawBackground(canvas)
        drawProgress(canvas)
    }

    private fun drawBackground(canvas: Canvas) {
        paint.color = resources.getColor(R.color.light_grey)
        canvas.drawRoundRect(
            0f, 0f, width.toFloat(), height.toFloat(), context.toPixel(4), context.toPixel(4), paint
        )
    }

    private fun drawProgress(canvas: Canvas) {
        paint.color = resources.getColor(R.color.colorPrimaryDark)
        canvas.drawRoundRect(
            0f, 0f, progress, height.toFloat(), context.toPixel(4), context.toPixel(4), paint
        )
    }

    fun animateProgress() {
        startDate?.also {
            if (duration != 0) {
                var percent = (Days.daysBetween(it.toLocalDate(), DateTime().toLocalDate()).days.toFloat()/duration.toFloat())
                if (percent > 1f) percent = 1f
                ValueAnimator.ofFloat(0f, width.toFloat()*percent).apply {
                    duration = resources.getInteger(android.R.integer.config_longAnimTime).toLong()
                    interpolator = FastOutSlowInInterpolator()
                    addUpdateListener { animator ->
                        progress = (animator.animatedValue as Float)
                        this@DateProgress.invalidate()
                    }
                    start()
                }
            }
        }
    }
}