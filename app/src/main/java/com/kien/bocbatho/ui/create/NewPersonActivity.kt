package com.kien.bocbatho.ui.create

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.kien.bocbatho.R
import com.kien.bocbatho.data.AppSharedPref
import com.kien.bocbatho.data.entities.Loan
import com.kien.bocbatho.data.entities.Person
import com.kien.bocbatho.ui.adapter.TextSuggestionAdapter
import com.kien.bocbatho.ui.base.BaseActivity
import com.kien.bocbatho.ui.widget.DatePickerDialogFragment
import com.kien.bocbatho.util.*
import com.kien.bocbatho.viewmodel.PersonViewModel
import kotlinx.android.synthetic.main.activity_new_person.*
import java.util.*

class NewPersonActivity: BaseActivity() {
    private lateinit var moneySuggestionAdapter: TextSuggestionAdapter
    private var startDate = Date()
    private lateinit var viewModel: PersonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_person)
        viewModel = ViewModelProviders.of(this@NewPersonActivity).get(PersonViewModel::class.java)

        moneySuggestionAdapter = TextSuggestionAdapter(this@NewPersonActivity, AppSharedPref.instance.moneySuggestions.toList())
        ed_money.setupMoneyInput()
        ed_money.setAdapter(moneySuggestionAdapter)
        ed_money.onTextChangedListener {
            val vd = it.toString().moneyToDouble()
            moneySuggestionAdapter.add((vd*1000).toMoneyString())
            moneySuggestionAdapter.add((vd*10000).toMoneyString())
            moneySuggestionAdapter.add((vd*100000).toMoneyString())
        }

        setupPicker(ed_start_date) {
            pickDate()
        }

        ed_money_per_day.setupMoneyInput()
        ed_money_per_day.setAdapter(moneySuggestionAdapter)
        ed_money_per_day.onTextChangedListener {
            val vd = it.toString().moneyToDouble()
            moneySuggestionAdapter.add((vd*10).toMoneyString())
            moneySuggestionAdapter.add((vd*100).toMoneyString())
        }

        btn_cancel.setOnClickListener {
            option("Huỷ thao tác thêm thành viên?",
                { d ->
                    d.dismiss()
                    this@NewPersonActivity.finish()
                }, { d -> d.dismiss()})
        }

        btn_save.setOnClickListener {
            val person = Person(
                name = ed_name.text.toString(),
                subName = ed_sub_name.text.toString(),
                phone = ed_phone.text.toString(),
                loans = arrayListOf()
            )

            val firstLoan = Loan(
                money = ed_money.text.toString().moneyToDouble(),
                moneyPerDay = ed_money_per_day.text.toString().moneyToDouble(),
                duration = ed_duration.text.toString().toAppInt(),
                startDate = startDate.time
            )

            person.loans?.add(firstLoan)
            viewModel.insert(person) {
                if (it != null) {
                    alert(it.message)
                } else {
                    toast("Thêm thành công")
                    this@NewPersonActivity.finish()
                }
            }
        }
    }

    private fun pickDate() {
        val picker = DatePickerDialogFragment()
        val selectedDate = startDate
        picker.setup(getString(R.string.ui_startDate), selectedDate, maxDate = Date())
        picker.show(supportFragmentManager, "startDate")
        picker.listener = object: DatePickerDialogFragment.OnSetInterface {
            override fun onPickerSet(sender: DatePickerDialogFragment, date: Date) {
                this@NewPersonActivity.startDate = date
                ed_start_date.setText(date.toString("dd/MM/yyyy"))
            }
        }
    }

    override fun onPause() {
        super.onPause()
        AppSharedPref.instance.moneySuggestions = moneySuggestionAdapter.data.toSet()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }
}