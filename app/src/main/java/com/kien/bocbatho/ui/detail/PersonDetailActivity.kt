package com.kien.bocbatho.ui.detail

import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.animation.*
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.ImageView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.kien.bocbatho.R
import com.kien.bocbatho.data.AppSharedPref
import com.kien.bocbatho.data.entities.Loan
import com.kien.bocbatho.data.entities.Person
import com.kien.bocbatho.ui.adapter.LoanRecyclerAdapter
import com.kien.bocbatho.ui.adapter.TextSuggestionAdapter
import com.kien.bocbatho.ui.base.BaseActivity
import com.kien.bocbatho.ui.widget.DatePickerDialogFragment
import com.kien.bocbatho.util.*
import com.kien.bocbatho.viewmodel.PersonViewModel
import kotlinx.android.synthetic.main.activity_detail.*
import java.util.*

class PersonDetailActivity: BaseActivity() {
    companion object {
        const val PERSON_ID = "person_id"
    }

    private var isAddingNewItem = false
        set(value) {
            val animIn = RotateAnimation(0f, 45f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
                duration = resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
                interpolator = OvershootInterpolator()
                isFillEnabled = true
                fillAfter = true
            }
            val animOut = RotateAnimation(45f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
                duration = resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
                interpolator = OvershootInterpolator()
                isFillEnabled = true
                fillAfter = true
            }
            field = value
            if (field) {
                btn_add.startAnimation(animIn)
                form_add.visibility = View.VISIBLE
            } else {
                btn_add.startAnimation(animOut)
                form_add.visibility = View.GONE
            }
        }
    private var person: Person? = null
        set(value) {
            value?.also {
                tv_name.text = it.name.let { name ->
                    if (name.isBlank()) {
                        tv_name.setTextColor(resources.getColor(R.color.black_secondary))
                        tv_name.setTypeface(tv_name.typeface, Typeface.ITALIC)
                        resources.getString(R.string.ui_no_name)
                    } else {
                        tv_name.setTextColor(resources.getColor(R.color.black))
                        tv_name.setTypeface(null, Typeface.NORMAL)
                        name
                    }
                }

                tv_sub_name.text = it.subName?.let { subName ->
                    if (subName.isBlank()) {
                        tv_sub_name.setTypeface(tv_name.typeface, Typeface.ITALIC)
                        resources.getString(R.string.ui_no_sub_name)
                    } else {
                        tv_sub_name.setTypeface(null, Typeface.NORMAL)
                        null
                    }
                }

                if (it.phone != null && it.phone!!.isNotBlank()) {
                    tv_phone.text = it.phone
                    tv_phone.setTextColor(resources.getColor(R.color.black))
                    tv_phone.setTypeface(null, Typeface.NORMAL)
                } else {
                    tv_phone.text = resources.getString(R.string.ui_no_phone)
                    tv_phone.setTextColor(resources.getColor(R.color.black_secondary))
                    tv_phone.setTypeface(tv_phone.typeface, Typeface.ITALIC)
                }

                tv_money.text = it.moneyPerDay.toMoneyString()

                it.loans?.also { loans ->
                    adapterLoan.setData(loans)
                }
            }
            field = value
        }


    private lateinit var viewModel: PersonViewModel
    private lateinit var adapterLoan: LoanRecyclerAdapter
    private var personId = -1L
    private lateinit var moneySuggestionAdapter: TextSuggestionAdapter
    private var startDate = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        viewModel = ViewModelProviders.of(this@PersonDetailActivity).get(PersonViewModel::class.java)
        personId = intent.getLongExtra(PERSON_ID, -1L)
        adapterLoan = LoanRecyclerAdapter(this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapterLoan

        moneySuggestionAdapter = TextSuggestionAdapter(this@PersonDetailActivity, AppSharedPref.instance.moneySuggestions.toList())
        ed_money.setupMoneyInput()
        ed_money.setAdapter(moneySuggestionAdapter)
        ed_money.onTextChangedListener {
            val vd = it.toString().moneyToDouble()
            moneySuggestionAdapter.add((vd*1000).toMoneyString())
            moneySuggestionAdapter.add((vd*10000).toMoneyString())
            moneySuggestionAdapter.add((vd*100000).toMoneyString())
        }

        setupPicker(ed_start_date) {
            pickDate()
        }

        ed_money_per_day.setupMoneyInput()
        ed_money_per_day.setAdapter(moneySuggestionAdapter)
        ed_money_per_day.onTextChangedListener {
            val vd = it.toString().moneyToDouble()
            moneySuggestionAdapter.add((vd*10).toMoneyString())
            moneySuggestionAdapter.add((vd*100).toMoneyString())
        }

        initListeners()
    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }

    private fun pickDate() {
        val picker = DatePickerDialogFragment()
        val selectedDate = startDate
        picker.setup(getString(R.string.ui_startDate), selectedDate, maxDate = Date())
        picker.show(supportFragmentManager, "startDate")
        picker.listener = object: DatePickerDialogFragment.OnSetInterface {
            override fun onPickerSet(sender: DatePickerDialogFragment, date: Date) {
                this@PersonDetailActivity.startDate = date
                ed_start_date.setText(date.toString(appDateFormat))
            }
        }
    }

    private fun reloadData() {
        if (personId >= 0) {
            viewModel.loadPerson(personId) { _, person ->
                swipe_refresh.isRefreshing = false
                if (person != null) {
                    this@PersonDetailActivity.person = person
                } else {
                    this@PersonDetailActivity.finish()
                }
            }
        } else {
            this@PersonDetailActivity.finish()
        }
    }

    private fun initListeners() {
        btn_add.setOnClickListener {
            isAddingNewItem = !isAddingNewItem
        }

        adapterLoan.setOnItemLongClickListener { _, pos ->
            person?.also { person ->
                person.loans?.also { loans ->
//                    loans[pos].hasPaid = true
//                    viewModel.update(person) {
//                        reloadData()
//                    }
                    showLoanEditDialog(loans[pos])
                }
            }
        }

        btn_ok.setOnClickListener {
            val loan = Loan(
                money = ed_money.text.toString().moneyToDouble(),
                moneyPerDay = ed_money_per_day.text.toString().moneyToDouble(),
                duration = ed_duration.text.toString().toAppInt(),
                startDate = startDate.time
            )
            person?.loans?.add(loan)
            person?.also {
                viewModel.update(it) {
                    reloadData()
                }
            }
            isAddingNewItem = !isAddingNewItem
        }

        layout_phone.setOnClickListener {
            person?.also { per ->
                per.phone?.also {  phone ->
                    if (phone.isNotBlank()) {
                        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                        startActivity(intent)
                    }
                }
            }
        }

        btn_back.setOnClickListener {
            this@PersonDetailActivity.finish()
        }

        btn_edit.setOnClickListener {
            person?.also {
                showPersonEditDialog(it)
            }
        }

        swipe_refresh.setOnRefreshListener {
            reloadData()
        }
    }

    private fun showLoanEditDialog(loan: Loan) {
        var startDate = Date(loan.startDate)
        Dialog(this, R.style.AppTheme_Dialog).apply {
            setContentView(R.layout.layout_edit_loan)
            val edMoney = findViewById<AutoCompleteTextView>(R.id.ed_money)
            val edStartDate = findViewById<TextInputEditText>(R.id.ed_start_date)
            edStartDate?.also {
                setupPicker(it) {
                    val picker = DatePickerDialogFragment()
                    val selectedDate = startDate
                    picker.setup(getString(R.string.ui_startDate), selectedDate, maxDate = Date())
                    picker.show(supportFragmentManager, "startDate")
                    picker.listener = object: DatePickerDialogFragment.OnSetInterface {
                        override fun onPickerSet(sender: DatePickerDialogFragment, date: Date) {
                            startDate = date
                            it.setText(date.toString(appDateFormat))
                        }
                    }
                }
            }
            val edDuration = findViewById<AutoCompleteTextView>(R.id.ed_duration)
            val edMoneyPerDay = findViewById<AutoCompleteTextView>(R.id.ed_money_per_day)
            val imageBG = findViewById<ImageView>(R.id.image_bg)
            val btnOk = findViewById<Button>(R.id.btn_ok)
            val btnPaid = findViewById<Button>(R.id.btn_paid)
            edMoney?.apply {
                setText(loan.money.toMoneyString())
                setupMoneyInput()
                setAdapter(moneySuggestionAdapter)
            }
            edStartDate?.setText(startDate.toString(appDateFormat))
            edDuration?.setText(loan.duration.toString())
            edMoneyPerDay?.apply {
                setText(loan.moneyPerDay.toMoneyString())
                setupMoneyInput()
                setAdapter(moneySuggestionAdapter)
            }
            if (loan.hasPaid) {
                imageBG?.visibility = View.VISIBLE
            } else {
                imageBG?.visibility = View.GONE
            }
            btnOk?.setOnClickListener {
                loan.money = edMoney!!.text!!.toString().moneyToDouble()
                loan.startDate = startDate.time
                loan.duration = edDuration!!.text!!.toString().toInt()
                loan.moneyPerDay = edMoneyPerDay!!.text!!.toString().moneyToDouble()
                viewModel.update(person!!) {
                    this.dismiss()
                    reloadData()
                }
            }
            btnPaid?.apply {
                if (loan.hasPaid) {
                    background = resources.getDrawable(R.drawable.rect_stroke_round_green_x2)
                    setTextColor(resources.getColor(R.color.green))
                    text = resources.getString(R.string.ui_un_paid)
                } else {
                    background = resources.getDrawable(R.drawable.rect_stroke_round_red_x2)
                    setTextColor(resources.getColor(R.color.red))
                    text = resources.getString(R.string.ui_paid)
                }
            }
            btnPaid?.setOnClickListener {
                loan.hasPaid = !loan.hasPaid
                viewModel.update(person!!) {
                    this.dismiss()
                    reloadData()
                }
            }
            show()
        }
    }

    private fun showPersonEditDialog(person: Person) {
        Dialog(this, R.style.AppTheme_Dialog).apply {
            setContentView(R.layout.layout_edit_person)
            val edName = findViewById<TextInputEditText>(R.id.ed_name)
            val edPhone = findViewById<TextInputEditText>(R.id.ed_phone)
            val btnOk = findViewById<Button>(R.id.btn_ok)
            edName?.setText(person.name)
            edPhone?.setText(person.phone)
            btnOk?.setOnClickListener {
                person.name = edName!!.text.toString()
                person.phone = edPhone!!.text.toString()
                viewModel.update(person) {
                    this.dismiss()
                    reloadData()
                }
            }
            show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }
}