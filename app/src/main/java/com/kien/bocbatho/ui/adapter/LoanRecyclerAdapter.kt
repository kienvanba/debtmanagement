package com.kien.bocbatho.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kien.bocbatho.R
import com.kien.bocbatho.data.entities.Loan
import com.kien.bocbatho.ui.widget.DateProgress
import com.kien.bocbatho.util.appDateFormat
import com.kien.bocbatho.util.toMoneyString
import com.kien.bocbatho.util.toString
import org.joda.time.DateTime
import java.util.*

class LoanRecyclerAdapter(private val context: Context): RecyclerView.Adapter<LoanViewHolder>() {
    private val items = arrayListOf<Loan>()
    private var clickListener: ((Loan) -> Unit)? = null
    private var longClickListener: ((Loan, Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoanViewHolder {
        return LoanViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_loan_item, parent, false))
    }

    override fun onBindViewHolder(holder: LoanViewHolder, position: Int) {
        val loan = items[position]

        holder.tvMoney.text = loan.money.toMoneyString()
        holder.tvMoneyPerDay.text = loan.moneyPerDay.toMoneyString()
        val startDate = Date(loan.startDate)
        holder.tvStartDate.text = "${startDate.toString(appDateFormat)} - ${startDate.let { DateTime(it).plusDays(loan.duration).toString(
            appDateFormat) }} (${loan.duration})"
        val startDateTime = DateTime(startDate)
        holder.tvEndDate.text = startDateTime.plusDays(loan.duration).toString(appDateFormat)
        if (loan.hasPaid) {
            holder.layoutInfo.background = context.resources.getDrawable(R.drawable.marker_paid)
            holder.tvMoney.setTextColor(context.resources.getColor(R.color.black_secondary))
            holder.tvMoneyPerDay.setTextColor(context.resources.getColor(R.color.black_secondary))
        } else {
            holder.layoutInfo.background = null
            holder.tvMoney.setTextColor(context.resources.getColor(R.color.black))
            holder.tvMoneyPerDay.setTextColor(context.resources.getColor(R.color.black))
        }
        holder.itemView.setOnClickListener {
            clickListener?.invoke(loan)
        }
        holder.itemView.setOnLongClickListener {
            longClickListener?.invoke(loan, position)
            true
        }
    }

    override fun getItemCount(): Int = items.size

    fun setData(data: List<Loan>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    fun setOnItemSelectListener(listener: (Loan) -> Unit) {
        this.clickListener = listener
    }

    fun setOnItemLongClickListener(listener: (Loan, Int) -> Unit) {
        this.longClickListener = listener
    }
}

class LoanViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val tvMoney: TextView = itemView.findViewById(R.id.tv_money)
    val tvMoneyPerDay: TextView = itemView.findViewById(R.id.tv_money_per_day)
    val tvStartDate: TextView = itemView.findViewById(R.id.tv_start_date)
    val tvEndDate: TextView = itemView.findViewById(R.id.tv_end_date)
    val layoutInfo: LinearLayout = itemView.findViewById(R.id.layout_info)
}