package com.kien.bocbatho.ui.base

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import android.content.DialogInterface
import android.content.DialogInterface.BUTTON_NEUTRAL
import androidx.appcompat.app.AlertDialog
import com.kien.bocbatho.R
import com.kien.bocbatho.ui.MainActivity



abstract class BaseActivity: AppCompatActivity() {

    protected fun toast(msg: String, shortToast: Boolean = true) {
        Toast.makeText(this, msg, if (shortToast) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    protected fun setupPicker(field: TextInputEditText, listener: () -> Unit) {
        field.setOnClickListener {
            dismissFocusIfNeed(field)
            listener()
        }
        field.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                dismissFocusIfNeed(field)
                listener()
            }
        }
    }

    private fun dismissFocusIfNeed(field: TextInputEditText) {
        if (field.isFocused) {
            field.clearFocus()
            hideKeyboard()
        }
    }

    protected fun hideKeyboard() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    protected fun alert(msg: String?) {
        msg?.also {
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle(resources.getString(R.string.app_name))
            alertDialog.setMessage(it)
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }
    }

    protected fun option(msg: String?, yes: ((DialogInterface) -> Unit)? = null, no: ((DialogInterface) -> Unit)? = null) {
        msg?.also {
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle(resources.getString(R.string.app_name))
            alertDialog.setMessage(it)
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES") { d, _ ->
                yes?.invoke(d)
            }
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO") { d, _ ->
                no?.invoke(d)
            }
            alertDialog.show()
        }
    }

    protected fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}