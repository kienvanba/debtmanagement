package com.kien.bocbatho.ui.widget

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.kien.bocbatho.util.Date
import java.util.*

class DatePickerDialogFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private var date: Date? = null
    private var minDate: Date? = null
    private var maxDate: Date? = null

    interface OnSetInterface {
        fun onPickerSet(sender: DatePickerDialogFragment, date: Date)
    }

    var listener: OnSetInterface? = null

    private var title: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = GregorianCalendar(Locale("en_US_POSIX"))
        val pickerDialog = DatePickerDialog(context!!, AlertDialog.THEME_HOLO_DARK,this, calendar.get(Calendar.YEAR), calendar.get(
            Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        date?.also {
            calendar.time = it
            pickerDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        }
        title?.also {
            pickerDialog.setTitle(it)
        }
        minDate?.also {
            pickerDialog.datePicker.minDate = it.time
        }
        maxDate?.also {
            pickerDialog.datePicker.maxDate = it.time
        }
        return pickerDialog
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        listener?.onPickerSet(this, Date(day = day, month = month, year = year))
    }

    fun setup(title: String? = null, date: Date? = null, minDate: Date? = null, maxDate: Date? = null) {
        this.title = title
        this.date = date
        this.minDate = minDate
        this.maxDate = maxDate
    }

}