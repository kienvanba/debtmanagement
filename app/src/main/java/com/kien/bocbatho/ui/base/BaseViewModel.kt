package com.kien.bocbatho.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.kien.bocbatho.DataRepository
import com.kien.bocbatho.data.AppDatabase
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel(application: Application): AndroidViewModel(application) {
    protected val compositeDisposable = CompositeDisposable()
    protected val repository: DataRepository

    init {
        val db = AppDatabase.getDatabase(application)
        repository = DataRepository(db)
    }

    fun dispose() {
        compositeDisposable.clear()
    }
}