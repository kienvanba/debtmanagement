package com.kien.bocbatho.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kien.bocbatho.R
import com.kien.bocbatho.ui.adapter.PersonRecyclerAdapter
import com.kien.bocbatho.ui.base.BaseActivity
import com.kien.bocbatho.ui.base.BaseAnimActivity
import com.kien.bocbatho.ui.create.NewPersonActivity
import com.kien.bocbatho.ui.detail.PersonDetailActivity
import com.kien.bocbatho.ui.widget.RecyclerItemDecor
import com.kien.bocbatho.util.center
import com.kien.bocbatho.util.onTextChangedListener
import com.kien.bocbatho.util.toMoneyString
import com.kien.bocbatho.util.toPixel
import com.kien.bocbatho.viewmodel.PersonViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity: BaseActivity() {
    private lateinit var adapterPerson: PersonRecyclerAdapter
    private lateinit var viewModel: PersonViewModel
    private var isExitting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(PersonViewModel::class.java)

        adapterPerson = PersonRecyclerAdapter(this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapterPerson
        recycler_view.addItemDecoration(RecyclerItemDecor(this@MainActivity, RecyclerItemDecor.VERTICAL_LIST, 56))

        reloadData()
        initListener()

        ed_search.setOnFocusChangeListener { view, focusing ->
            layout_search.cardElevation = if (focusing) {
                toPixel(4)
            } else {
                run {
                    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
                toPixel(0)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }

    private fun reloadData() {
        ed_search.clearFocus()
        viewModel.loadAllPerson { error, persons ->
            swipe_refresh.isRefreshing = false
            if (persons != null) {
                var total = 0.0
                persons.forEach { p ->
                    total += p.moneyPerDay
                }
                tv_person_count.text = persons.size.toString()
                tv_money.text = total.toMoneyString()

                adapterPerson.setData(persons)
            } else {
                alert(error?.message)
            }
        }
    }

    private fun initListener() {
        swipe_refresh.setOnRefreshListener {
            reloadData()
        }

        ed_search.onTextChangedListener {
            adapterPerson.filter.filter(it)
        }

        adapterPerson.setOnItemClickListener { person ->
            val intent = Intent(this@MainActivity, PersonDetailActivity::class.java)
            intent.putExtra(PersonDetailActivity.PERSON_ID, person.id)
            startActivity(intent)
        }

        adapterPerson.setOnItemDeleteListener { person ->
            viewModel.delete(person) {
                reloadData()
            }
        }

        layout_add.setOnClickListener {
            val intent = Intent(this@MainActivity, NewPersonActivity::class.java)
            intent.putExtra(BaseAnimActivity.animAtX, layout_add.center().x)
            intent.putExtra(BaseAnimActivity.animAtY, layout_add.center().y)
            startActivity(intent)
        }
    }

    private val exitHandler = Handler()
    override fun onBackPressed() {
        when {
            adapterPerson.isDeleting -> adapterPerson.deleteOff()
            ed_search.text.isNotBlank() -> ed_search.setText("")
            isExitting -> {
                exitHandler.removeCallbacksAndMessages(null)
                super.onBackPressed()
            }
            else -> {
                isExitting = true
                toast("Nhấn thêm lần nữa để thoát ứng dụng")
                exitHandler.postDelayed({
                    isExitting = false
                }, 2000)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }
}