package com.kien.bocbatho.model

class AppError(var code: Int, var message: String) {
    constructor(throwable: Throwable): this(0, throwable.message ?: "Unknown")
}