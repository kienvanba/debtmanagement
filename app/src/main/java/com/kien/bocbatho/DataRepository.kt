package com.kien.bocbatho

import com.kien.bocbatho.data.AppDatabase
import com.kien.bocbatho.data.entities.Person
import io.reactivex.Completable
import io.reactivex.Single

class DataRepository(private val database: AppDatabase) {

    fun loadAllPerson() = database.personDao().loadAllPerson()

    fun loadPerson(id: Long) = database.personDao().loadPerson(id)

    fun insertPerson(person: Person): Single<Long> {
        return Single.create { emitter ->
            val id = database.personDao().insert(person)
            if (id < 0) {
                emitter.onError(Throwable("Thêm nợ nần thất bại (mã nợ $id)"))
            } else {
                emitter.onSuccess(id)
            }
        }
    }

    fun deletePerson(person: Person): Completable {
        return Completable.create { emitter ->
            database.personDao().delete(person)
            emitter.onComplete()
        }
    }

    fun updatePerson(person: Person): Completable {
        return Completable.create { emitter ->
            database.personDao().update(person)
            emitter.onComplete()
        }
    }
}