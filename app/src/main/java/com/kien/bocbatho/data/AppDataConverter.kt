package com.kien.bocbatho.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kien.bocbatho.data.entities.Loan

class AppDataConverter {
    @TypeConverter
    fun toLoanJson(loans: ArrayList<Loan>?): String? {
        return loans?.let {
            val type = object: TypeToken<ArrayList<Loan>>() {}.type
            Gson().toJson(loans, type)
        }
    }

    @TypeConverter
    fun fromLoanJson(loans: String?): ArrayList<Loan>? {
        return loans?.let {
            val type = object: TypeToken<ArrayList<Loan>>() {}.type
            Gson().fromJson(loans, type)
        }
    }
}