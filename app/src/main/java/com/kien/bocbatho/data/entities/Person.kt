package com.kien.bocbatho.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*
import kotlin.collections.ArrayList

@Entity(tableName = "person")
class Person(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "subName") var subName: String? = null,
    @ColumnInfo(name = "phone") var phone: String? = null,
    @ColumnInfo(name = "loans") val loans: ArrayList<Loan>? = null,
    @ColumnInfo(name = "createDate") val createDate: Long = Date().time
) {
    val moneyPerDay: Double
        get() {
            return if (loans != null) {
                var total = 0.0
                loans.forEach { loan ->
                    if (!loan.hasPaid) {
                        total += loan.moneyPerDay
                    }
                }
                total
            } else {
                0.0
            }
        }
}