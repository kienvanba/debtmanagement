package com.kien.bocbatho.data

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kien.bocbatho.data.dao.PersonDao
import com.kien.bocbatho.data.entities.Loan
import com.kien.bocbatho.data.entities.Person

@Database(entities = [Person::class], version = 2)
@TypeConverters(AppDataConverter::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun personDao(): PersonDao

    private val isDBCreated: MutableLiveData<Boolean> = MutableLiveData()

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "bocbatho_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}