package com.kien.bocbatho.data

import android.content.Context
import android.content.SharedPreferences
import com.kien.bocbatho.App

class AppSharedPref {
    private val file = "bbh-file"
    private val dk = "_duration_key"
    private val msk = "_money_suggestions_key"

    companion object {
        val instance = AppSharedPref()
    }

    private val default: SharedPreferences

    init {
        default = App.instance.getSharedPreferences(file, Context.MODE_PRIVATE)
    }

    var duration: Set<String> = emptySet()
        get() {
            field = default.getStringSet(dk, null) ?: emptySet()
            return field
        }
        set(value) {
            field = value
            default.edit().putStringSet(dk, field).apply()
        }

    var moneySuggestions: Set<String> = emptySet()
        get() {
            field = default.getStringSet(msk, null) ?: emptySet()
            return field
        }
        set(value) {
            field = value
            default.edit().putStringSet(msk, field).apply()
        }
}