package com.kien.bocbatho.data.dao

import androidx.room.*
import com.kien.bocbatho.data.entities.Person
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface PersonDao {
    @Query("SELECT * FROM person ORDER BY name ASC")
    fun loadAllPerson(): Single<List<Person>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(listPeople: List<Person>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(person: Person): Long

    @Query("SELECT * FROM person WHERE id = :personId")
    fun loadPerson(personId: Long): Single<Person>

    @Update
    fun update(person: Person)

    @Delete
    fun delete(person: Person)

    @Query("select * from person where name like :text or subName like :text")
    fun searchAllPerson(text: String): Single<List<Person>>
}