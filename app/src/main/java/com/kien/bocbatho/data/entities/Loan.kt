package com.kien.bocbatho.data.entities

import com.google.gson.annotations.SerializedName
import java.util.*

data class Loan(
    @SerializedName("_id") val id: Long = 0,
    @SerializedName("money") var money: Double,
    @SerializedName("money_per_day") var moneyPerDay: Double,
    @SerializedName("duration") var duration: Int,
    @SerializedName("start_date") var startDate: Long = Date().time,
    @SerializedName("paid") var hasPaid: Boolean = false
)