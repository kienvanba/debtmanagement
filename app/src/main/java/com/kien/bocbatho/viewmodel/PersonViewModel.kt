package com.kien.bocbatho.viewmodel

import android.app.Application
import com.kien.bocbatho.data.entities.Person
import com.kien.bocbatho.model.AppError
import com.kien.bocbatho.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PersonViewModel(application: Application): BaseViewModel(application) {

    fun loadAllPerson(completion: (error: AppError?, persons: List<Person>?) -> Unit) {
        compositeDisposable.add(repository.loadAllPerson()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ persons ->
                completion(null, persons)
            }, {
                completion(AppError(it), null)
            }))
    }

    fun loadPerson(id: Long, completion: (error: AppError?, person: Person?) -> Unit) {
        compositeDisposable.add(repository.loadPerson(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                completion(null, it)
            }, {
                completion(AppError(it), null)
            }))
    }

    fun insert(person: Person, completion: (error: AppError?) -> Unit) {
        compositeDisposable.add(repository.insertPerson(person)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                completion(null)
            }, {
                completion(AppError(it))
            }))
    }

    fun update(person: Person, completion: () -> Unit) {
        compositeDisposable.add(repository.updatePerson(person)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                completion()
            }, {
                completion()
            }))
    }

    fun delete(person: Person, completion: () -> Unit) {
        compositeDisposable.add(repository.deletePerson(person)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                completion()
            }, {
                completion()
            }))
    }
}