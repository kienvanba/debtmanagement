package com.kien.bocbatho.util

import android.util.Log

const val defIntValue: Int = -1204
const val appDateFormat = "dd/MM/yyyy"

fun log(msg: String, tag: String = "BBH") {
    Log.d(tag, msg)
}