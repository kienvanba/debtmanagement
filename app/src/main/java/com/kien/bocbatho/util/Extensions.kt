package com.kien.bocbatho.util

import android.R.layout.simple_dropdown_item_1line
import android.content.Context
import android.graphics.Point
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

fun EditText.onTextChangedListener(onChanged: (editable: CharSequence?) -> Unit) {
    addTextChangedListener(object: TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            onChanged(p0)
        }
        override fun afterTextChanged(p0: Editable?) {}
    })
}

fun Double.toMoneyString(): String {
//    val formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("vi-VN"))
//    return formatter.format(this)
    return String.format("%,.0f", this)
}

fun String.moneyToDouble(): Double {
    val extractNumberPattern = "[^0-9]+"
    val valueAfterExtract = this.replace(Regex(extractNumberPattern), "")
    return if (valueAfterExtract.isNotBlank()) valueAfterExtract.toDouble() else 0.0
}

fun String.toAppInt(): Int {
    if (isNullOrEmpty() || isBlank()) {
        return 0
    }
    return toInt()
}

fun Int.isNotDefValue(): Boolean = this != defIntValue

fun AutoCompleteTextView.addMoneySuggestion(context: Context) {
    onTextChangedListener {chars ->
        val value = chars.toString().moneyToDouble()
        val adapter = ArrayAdapter(
            context,
            simple_dropdown_item_1line,
            arrayListOf((value*10000).toMoneyString(), (value*100000).toMoneyString(), (value*1000000).toMoneyString())
        )
        setAdapter(adapter)
    }
}

fun AutoCompleteTextView.addSuggestions(context: Context, suggestions: List<String>) {
    val adapter = ArrayAdapter(
        context,
        simple_dropdown_item_1line,
        suggestions
    )
    setAdapter(adapter)
}

fun EditText.setupMoneyInput() {
    addTextChangedListener(object: TextWatcher {
        var current = ""
        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (p0.toString() != current) {
                removeTextChangedListener(this)
                val input = p0.toString()
                val moneyAsDouble = input.moneyToDouble()
                val moneyAsText = moneyAsDouble.toMoneyString()
                current = moneyAsText
                setText(moneyAsText)
                setSelection(moneyAsText.length)
                addTextChangedListener(this)
            }
        }
    })
}

fun View.center(): Point {
    val x = this.x + this.width/2
    val y = this.y + this.height/2
    return Point(x.toInt(), y.toInt())
}

fun Date(second: Int = 0, minute: Int = 0, hour: Int = 0, day: Int, month: Int, year: Int) : Date {
    val calendar = GregorianCalendar(Locale("en_US_POSIX"))
    calendar.set(Calendar.MILLISECOND, 0)
    calendar.set(Calendar.SECOND, second)
    calendar.set(Calendar.MINUTE, minute)
    calendar.set(Calendar.HOUR_OF_DAY, hour)
    calendar.set(Calendar.DAY_OF_MONTH, day)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.YEAR, year)
    return calendar.time
}

fun Date.calendar(local: String = "en_US_POSIX") : Calendar {
    val calendar = GregorianCalendar(Locale(local))
    calendar.set(Calendar.MILLISECOND, 0)
    calendar.time = this
    return calendar
}

fun Date.add(second: Int = 0, minute: Int = 0, hour: Int = 0, day: Int = 0, month: Int = 0, year: Int = 0) : Date {
    val calendar = calendar()
    calendar.add(Calendar.SECOND, second)
    calendar.add(Calendar.MINUTE, minute)
    calendar.add(Calendar.HOUR, hour)
    calendar.add(Calendar.DAY_OF_MONTH, day)
    calendar.add(Calendar.MONTH, month)
    calendar.add(Calendar.YEAR, year)
    return calendar.time
}

fun Date.toString(format: String): String {
    val formatter = SimpleDateFormat(format, Locale.getDefault())
    formatter.calendar = GregorianCalendar(Locale.getDefault())
    return formatter.format(this)
}

fun Date.year(): Int {
    return calendar().get(Calendar.YEAR)
}

fun Date.yearOld(): Int {
    return Date().year() - this.year()
}

fun Context.toPixel(dp: Int) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics)